<?php


$full = elgg_extract('full_view', $vars, false);
$entity = elgg_extract('entity', $vars);

if (!$entity) {
    return;
}

$metadata = elgg_view_menu('entity', array(
    'entity' => $entity,
    'handler' => 'examples',
    'sort_by' => 'priority',
    'class' => 'elgg-menu-hz',
));


$title = elgg_format_element('a', [
    'href' => $entity->getURL()
], ' ' . $entity->getDisplayName());


if ($full) {

    echo elgg_view('object/elements/full', array(
        'entity' => $entity,
        'icon' => $user,
        'summary' => $summary,
        'body' => $body,
        'metadata' => $metadata
    ));

} else {

    // brief view
    $vacancy_img = elgg_view_entity_icon($owner, 'small');

    $params = array(
        'entity' => $entity,
        'metadata' => $metadata,
        'title' => $title,
        'subtitle' => '<b>' . elgg_echo('example:genre') . ':</b> ' . ucfirst($entity->genre)
    );

    $list_body = elgg_view('object/elements/summary', $params);
    echo elgg_view_image_block($vacancy_img, $list_body);


}