<?php

// Set all variables empty
$title = '';
$name = '';
$genre = [];

$access_id = ACCESS_PUBLIC;
$guid = elgg_get_logged_in_user_guid();

$new = true;

// Write all variables when an entity is found
$entity = elgg_extract('entity', $vars);


if (elgg_instanceof($entity, 'object', 'example')) {
    /* @var ElggEntity $entity */
    $title = $entity->title;
    $name = $entity->name;
    $genre = $entity->genre;
    $access_id = $entity->access_id;
    $container_guid = $entity->container_guid;

    $positions = $entity->getEntitiesFromRelationship([
        'from' => 'position',
        'limit' => 0
    ], false, 0);

    $new = false;
}

if (elgg_is_sticky_form('example')) {
    extract(elgg_get_sticky_values('example'));
    elgg_clear_sticky_form('example');
}


$fields = [
    [
        'type' => 'text',
        'name' => 'title',
        'value' => $title,
        'label' => elgg_echo('title'),
        'required' => true
    ],
    [
        'type' => 'text',
        'name' => 'name',
        'value' => $name,
        'label' => elgg_echo('example:name'),
        'required' => true
    ],
    [
        'type' => 'dropdown',
        'name' => 'genre',
        'label' => elgg_echo('example:genre'),
        'options_values' => example_genre(),
        'value' => $status,
        'required' => true
    ],
    [
        'type' => 'longtext',
        'name' => 'description',
        'value' => $description,
        'label' => elgg_echo('description'),
        'required' => true
    ],
    [
        'type' => 'file',
        'name' => 'icon',
        'label' => elgg_echo('example:uploadimages'),
        'required' => false,
        'help' => (!$new) ? elgg_echo('example:form:not_new:image') : ''
    ],
    [
        'type' => 'access',
        'name' => 'access_id',
        'value' => $access_id,
        'entity_type' => 'object',
        'entity_subtype' => 'vacancy',
        'label' => elgg_echo('access'),
        'required' => true
    ],
    [
        'type' => 'hidden',
        'name' => 'container_guid',
        'value' => $container_guid,
    ],
    [
        'type' => 'hidden',
        'name' => 'guid',
        'value' => (!$new) ? $entity->getGUID() : '',
    ],
    [
        'type' => 'submit',
        'value' => elgg_echo('example:add'),
        'field_class' => 'elgg-foot',
    ]
];

foreach ($fields as $field) {
    $type = elgg_extract('type', $field, 'text');
    unset($field['type']);
    if ($type == 'example') {
        $field = $vars;
    }
    echo elgg_view_input($type, $field);
}