<?php

admin_gatekeeper();


// Get the post, if it exists
$id = (int) get_input('guid');

$entity = get_entity($id);


$title = elgg_echo('example:edit');
$content = elgg_view_form('example/add', array('enctype' => 'multipart/form-data'));

// Show example sidebar
$sidebar = elgg_view("example/sidebar/general");

$params = array(
    'content' => $content,
    'title' => $title,
    'sidebar' => $sidebar,
);

$body = elgg_view_layout('one_sidebar', $params);

echo elgg_view_page($title, $body);

