<?php
//gatekeeper();

$title = 'example';

$options = array(
    'types' => 'object',
    'subtypes' => 'example',
    'limit' => 20,
    'metadata_name_value_pairs' => [
        [
            'name' => 'genre',
            'value' => '1',
            'operand' => '='
        ]
    ],
    'full_view' => false,
    'pagination' => true,
    'list_type_toggle' => false,
);

$content = elgg_list_entities_from_metadata($options);

elgg_pop_breadcrumb();
elgg_push_breadcrumb(elgg_echo('example'));
elgg_register_title_button();

$body .= elgg_view_layout('content', array(
    'filter' => '',
   'filter_context' => 'all',
    'content' => $content,
    'title' => $title,
    'sidebar' => elgg_view('example/sidebar'),
    'class' => 'elgg-river-layout'
));

echo elgg_view_page($title, $body);


?>