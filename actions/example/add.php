<?php

admin_gatekeeper();

elgg_make_sticky_form('example');

// Get input data
$title = get_input('title');
$name = get_input('name');
$description = get_input('description');
$genre = get_input('genre');
$guid = get_input('guid');

// Make sure the title / description aren't blank
if (empty($title) || empty($name)) {
    register_error(elgg_echo('example:blank'));
    forward(REFERER);
}

$app = get_entity($guid);

if (!elgg_instanceof($app, 'object', 'example')) {
    // Initialise a new ElggObject
    $app = new ElggObject();
    $app->subtype = "example";
    // Set title information
    $app->owner_guid = elgg_get_logged_in_user_guid();
}

$app->title = $title;
$app->name = $name;
$app->description = $description;
$app->genre = $genre;

$app->save();

system_message(elgg_echo('vacancy:posted'));

// Forward to the jobs page
forward(elgg_get_site_url() . 'example/view/' . $app->getGUID());


