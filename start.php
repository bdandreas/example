<?php

elgg_register_event_handler('init', 'system', 'example_init');

/**
 *
 */
function example_init()
{
    elgg_register_page_handler('example', 'handler');
    $action_base = elgg_get_plugins_path() . 'example/actions';
    elgg_register_action("example/add", "$action_base/example/add.php");
    elgg_register_action("example/edit", "$action_base/example/edit.php");
    elgg_register_entity_type('object', 'example');
}

function handler($task)
{
    elgg_push_breadcrumb(elgg_echo('example'), 'example/all');

    $base_dir = __DIR__ . '/pages/example';

    if ($task[0] == ''){
        include $base_dir."/index.php";
    }

    if ($task[0] == 'view'){
        include $base_dir."/index.php";
    }
    if ($task[0] == 'add'){
        include $base_dir."/add.php";
    }
    return true;
}

/**
 *
 */
function example_genre()
{
    return array(
       '1' => 'Pop',
       '2' => 'Jazz',
       '3' => 'Klassiek',
    );


}